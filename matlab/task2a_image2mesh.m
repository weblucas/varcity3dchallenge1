% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Learning Where To Classify In Multi-View Semantic Segmentation
% ETHZ RueMonge 2014 dataset + load / evaluation code
%
% Learning Where To Classify In Multi-View Semantic Segmentation
% ECCV 2014, Zurich, Switzerland. (PDF, poster, project)
% H. Riemenschneider, A. Bodis-Szomoru, J. Weissenberg, L. Van Gool
%
% http://varcity.eu/3dchallenge/
% http://www.vision.ee.ethz.ch/~rhayko/paper/eccv2014_riemenschneider_multiviewsemseg/
%
% Please cite the above work if you use any of the code or dataset.
%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% TASK 2 - Mesh Labelling - collect or reason in 3d to label mesh
% SUMMARY

% THREE MODES
% 2a - Image2Mesh Labelling - collect images to label mesh (THIS CODE)
% 2b - Pointcloud2Mesh Labelling - collect pointcloud to label mesh
% 2c - Mesh2Mesh Labelling - collect mesh results directy

% THREE PARTS
% YOUR OWN TRAINING OUTSIDE?
%   clone 'eccv2014' and 'imagemap2mesh' / 'imagegco2mesh' folders
%   produce .mat or .png files and use code to load
%   produce data.predictprob [202 x 8 x numFaces]
%   select fusion method

% YOUR OWN TRAINING HERE?
%   see examples in task2c_mesh2mesh.m
%
% YOUR OWN FUSION?
%   load('imageprob2mesh_data.mat','data')
%
% YOUR OWN 3D MESH LABELING?
%   dataset.mesh_predict == [1 x num_face] labels ranging from [0...num_label]
%   score = evaluation_multilabel(dataset.mesh_gt_test-1, dataset.mesh_predict-1, dataset_labelskip)

close all
clear
clc


%% SETUP PATHS
addpath(genpath('.'))
%addpath(genpath('thirdparty/gco'))

method = 'imagegco2mesh'; % COLLECT GCO LABEL IMG INTO MESH
method = 'imagemap2mesh'; % COLLECT MAP LABEL IMG INTO MESH
method = 'imageprob2mesh'; % COLLECT RAW PROBS INTO MESH

dataset = load_dataset('ruemonge428','results_eccv2014', method);

%% DATA COLLECTION

if(~exist(dataset.predicttestfiledata,'file'))
    display(['processing ' dataset.predicttestfiledata] )
    t=cputime;
    data = mesh_collect_files (dataset, dataset.test.list_idx, dataset.test.num_view, dataset.num_label, dataset.num_face, dataset.cm, method);
    timing.load=cputime-t;
    save(dataset.predicttestfiledata,'data','-v7.3')
else
    display(['loading ' dataset.predicttestfiledata] )
    %t=cputime;
    load(dataset.predicttestfiledata,'data')
    %timing.loaddata=cputime-t;
end


%% DATA FUSION (BASELINE ONLY)
display(['fusing data...'])

switch(method)
    case 'imageprob2mesh'
        t=cputime;
        if(length(size(data.predictprob))>2)
            data.cams = squeeze(max(sum(data.area>0,1),[],2))';
%            figure, hist(data.cams(data.cams>0),[1:20])

            % eccv 2014 paper baselines - all probs
            % sum over all probabilities from all views
            data.predictprob_sum = squeeze(sum(data.predictprob,1))';
            % average over all probabilities from all views
            data.predictprob_mean = bsxfun(@times, squeeze(sum(data.predictprob,1))', 1./max(data.cams,1)');
            % maximum over all probabilities from all views
            data.predictprob_max = squeeze(max(data.predictprob,[],1))';
            
            % eccv 2014 paper - select view
            % only max area over from all views
            % only min angle over from all views
            % only learned mapping over from all views
                        
        else
            data.predictprob_sum = data.predictprob';
        end
        dataset.unary = conversion_likelihood2cost(data.predictprob_sum', false);
        timing.fusion=cputime-t;
        
    case {'imagemap2mesh', 'imagegco2mesh'}
        t=cputime;
        dataset.unary = conversion_likelihood2cost(data.predicttest, false);
        timing.fusion=cputime-t;
end


%% GENERATE 3D GRAPH RESULT
display(['optimizing graph labelling...'])

dataset.alpha = 0.0; % MAP MAX result (without any regularization)
dataset.alpha = 0.1; % GRAPHCUT regularization

t=cputime;
dataset.pairwise = mesh_adjacency_onehit(dataset.face, dataset.vertex);
dataset.mesh_predict = tool_graphcut_gco(dataset.unary, dataset.pairwise, dataset.alpha, [], [], true, 100000)';
timing.optimization=cputime-t;




%% EVALUATION TASK 2a - Image2Mesh Labelling - collect images to label mesh (THIS CODE)
display(['evaluating results...'])

tabulate(dataset.mesh_gt_test)
tabulate(dataset.mesh_predict)

t=cputime;
score = evaluation_multilabel(dataset.mesh_gt_test-1, dataset.mesh_predict-1, dataset.labelskipidx, dataset.num_label);
timing.evaluation=cputime-t;


display(['exporting results...'])
t=cputime;
score
labels_str = dataset.labels_str;
labels_str(dataset.labelskipidx) = [];
evaluation_confusion(score.confusion, labels_str, [dataset.name ' ' method]);
saveas(gcf,[dataset.predicttestfilescore(1:end-4) '.png'])
mesh_predict = dataset.mesh_predict;
save(dataset.predicttestfilescore,'score','timing','mesh_predict')
export_ply_simple(dataset.predicttestfileply, dataset.vertex, dataset.face, dataset.cm(mesh_predict,:)'*255)
timing.export=cputime-t;


display(['done!'])
diary off;




