function evaluation_multilabel_printall(score, labels, labels_unused)
% evaluation_multilabel_printall (score, labels) displays the accuracies.
% Prints a nice wrapper for a set of  score structs.
%
% author: hayko riemenschneider, 2015
%
% See also evaluation_multilabel, evaluation_multilabel_print

if(exist('labels','var'))
    labels(labels_unused)=[];
    strstr = 'avg.  ';
    for idx = 1:length(labels)
        str = labels{idx}(1:min(5,length(labels{idx})));
        strstr= [strstr str repmat(' ',1,6-length(str))];
    end
    display(['' strstr])
end

display([num2str(score.mean_pascal,'%02.2f ') ' ' num2str(score.accuracy_pascal,'%02.2f ')])
%display([num2str(score.mean_class,'%02.2f ') ' ' num2str(score.accuracy_classwise,'%02.2f ')])


