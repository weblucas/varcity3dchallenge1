%% debug: loop over alpha to find best pairwise (brute force sanity check)

alpha_range = 0.0:0.1:2;
dataset.pairwise = mesh_adjacency_onehit(dataset.face, dataset.vertex);
for alpha = alpha_range
    dataset.alpha = alpha;
    dataset.mesh_predict = tool_graphcut_gco(dataset.unary, dataset.pairwise, dataset.alpha, [], [], true, 100000)';
    score = evaluation_multilabel(dataset.mesh_gt_test-1, dataset.mesh_predict-1, dataset.labelskipidx, dataset.num_label);
    jaccard(round(alpha*10+1))=score.mean_pascal;
end
figure, plot(alpha_range, jaccard)
xlabel('pairwise smooth')
ylabel('IOU jaccard index [%]')
grid on

[m,v]=max(jaccard)
alpha_range(v)