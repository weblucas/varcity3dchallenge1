function data = mesh_collect_files (path, file_str_idx, numViews, numLabels, numFaces, cm, type)
% data = mesh_collect_files (path, file_str_idx, numViews, numLabels, numFaces, type)
%
% Collect data from file on disk to data storage for multiview face storage.
% Runs through al images in the file_str_idx list and collects it data via the triangle index.
%
% Sources:
%       images          rgb mean color from images (.jpg)
%       train           label hist from GT training images (.png)
%       test            label hist from GT testing images (.png)
%       predictprob     probablities from predicted label probabilities (.mat)
%       predicttest     label hist from predicted label maps (.png)
%
% author: hayko riemenschneider, 2014

%% DATA ALLOCATION
if(numLabels > 8); numLabels = 8; display('labels =8 now'); end;
data.area = zeros(numViews,1,numFaces,'single');

flag_fullprobs = true;

switch (type)
    
    case 'images'
        data.col = zeros(numViews,3,numFaces,'single');
    case 'train'
        data.train = zeros(numLabels,numFaces,'single');
    case 'test'
        data.test = zeros(numLabels,numFaces,'single');
    case 'imageprob2mesh'
        % debug: choice between average and individual prob
        if(flag_fullprobs)
            data.predictprob = zeros(numViews,numLabels,numFaces,'single');
            %data.predictprob = zeros(numViews,numLabels,numFaces,'double'); % needs more than 32 gbyte of memory
            %data.predictprob = sparse(numViews,numLabels,numFaces); % no 3d sparse matrix in matlab
        else
            data.predictprob = zeros(numLabels,numFaces,'double');
        end
    case {'imagemap2mesh','imagegco2mesh'}
        data.predicttest = zeros(numLabels,numFaces,'single');
        
end

%% DATA LOADING

for file_idx = 1: numViews
    
    filename = [file_str_idx{file_idx} '.jpg'];
    progressbartime(file_idx, numViews, type)
    % display (['processing #' num2str(file_idx) '/' num2str(numViews) ': ' filename])
    
    try
        im_idx = load([path.index filename(1:end-4) '.txt']);
    catch
        try
            im_idx = load([path.index filename(1:end-4) '_1.mat']);
            im_idx = im_idx.index_map;
        catch
            try
                im_idx = load([path.index filename(1:end-4) '.mat']);
                im_idx = im_idx.index_map;
            catch
                continue;
            end
        end
    end
    im_idx = im_idx+1; % get rid of index 0: NOTE: undone LATER
    
    % collect face area
    sumperindex = accumarray (im_idx(:), 1, [numFaces+1 1]);
    data.area(file_idx,:) = (sumperindex(2:end)); % LATER: GET RID OF +1
    
    switch(type)
        % ----------------------------------------------------------------------------------------------
        case 'images'
            % collect mean color from images
            im_rgb = imread([path.images filename(1:end-4) '.jpg']);
            data.col(file_idx,:,:) = mesh_collect_raw(im_rgb, im_idx, numFaces);
            
            % ----------------------------------------------------------------------------------------------
        case 'train'
            
            % collect histogram over training labelmaps
            try
                im_train_rgb = imread([path.train filename(1:end-4) '.png']);
                data.train = data.train + mesh_collect_labelmap(im_train_rgb, im_idx, cm, numFaces);
            catch
                display(filename(1:end-4))
                % no training images available for this part of the mesh
            end
            
            % ----------------------------------------------------------------------------------------------
        case 'test'
            
            % collect histogram over testing labelmaps
            try
                im_test_rgb = imread([path.test filename(1:end-4) '.png']);
                data.test = data.test + mesh_collect_labelmap(im_test_rgb, im_idx, cm, numFaces);
            catch
                
                display(filename(1:end-4))
                % no testing images available for this part of the mesh
            end
            
            
        case 'imageprob2mesh'
            % ----------------------------------------------------------------------------------------------
            % collect mean prob from images
            try
                im_prob = load([path.predicttest  filename(1:end-4) '.mat']);
                if(isfield(im_prob,'im_res_probs'))
                    im_prob = single(im_prob.im_res_probs);
                end
                if(isfield(im_prob,'im_prob_test'))
                    im_prob = single(im_prob.im_prob_test(:,:,1:8));
                end
                %im_prob = save(['../data/probraw/' filename(1:end-4) '.mat'],'im_prob','-v7.3');
                
                % debug: choice between average and individual prob
                 if(flag_fullprobs)
                data.predictprob(file_idx,:,:) = mesh_collect_raw(im_prob, im_idx, numFaces);
                 else
                data.predictprob = data.predictprob + mesh_collect_raw(im_prob, im_idx, numFaces);
                 end
            catch
                display(filename(1:end-4))
                
                % no prediction images available for this part of the mesh
            end
            
        case {'imagemap2mesh','imagegco2mesh'}
            % ----------------------------------------------------------------------------------------------
            % collect histogram over testing labelmaps
            try
                im_test_rgb = imread([path.predicttest filename(1:end-4) '.png']);
                data.predicttest = data.predicttest + mesh_collect_labelmap(im_test_rgb, im_idx, cm, numFaces);
            catch
                display(filename(1:end-4))
                % no testing images available for this part of the mesh
            end
            
            
            
    end     % switch
    
end

