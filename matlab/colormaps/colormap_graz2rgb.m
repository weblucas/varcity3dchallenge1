function [labelmap, label_str, label_unused] = colormap_graz2rgb (visualize)

label_str = {'void','window','wall','balcony','door','roof','sky','shop'};
label_unused = [1];

% NAME:     Void Window Wall Balcony Door Roof Sky Shop ROAD
% LABELMAP  0  1       2       3      4      5   6   7     8
% MATLAB    1  2       3       4      5      6   7   8     9

labelmap = [
0	0	0	;
255	0	0	;
255	255	0	;
128	0	255	;
255	128	0	;
0	0	255	;
128	255	255	;
0	255	0	; 
]/255;


if(~exist('visualize','var')), visualize = false; end;

if(visualize)
    figure
    im_labelmap = viz_labelmap(labelmap, label_str);
    imshow (im_labelmap)
%     im_labels = imresize(repmat((1:size(labelmap,1))',1,10), 50, 'nearest');
%    imshow(label2rgb(im_labels,labelmap))
%     for idx = 1: size(labelmap, 1)
%         text(200,25+50*(idx-1),label_str{idx},'Color','k','FontSize', 20)
%     end
%    saveas(gcf,[mfilename '.png'])
    imwrite(im_labelmap,[mfilename '.png'])
        
end

