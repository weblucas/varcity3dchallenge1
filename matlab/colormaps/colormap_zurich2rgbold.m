function [labelmap, label_str, label_skipidx] = colormap_zurich2rgbold

label_str = {'void','window','wall','balcony','door','roof','sky','shop','shutter','road'};


% NAME:     Void Window Wall Balcony Door Roof Sky Shop Shutter ROAD
% LABELMAP  0  1       2       3      4      5   6   7     8    9
% MATLAB    1  2       3       4      5      6   7   8     9   10


label_skipidx = [1 10];

% 0	Outlier	0	0	0
% 1	Window	255	0	0
% 2	Wall	255	255	0
% 3	Balcony	128	0	255
% 4	Door	255	128	0
% 5	Roof	0	0	255
% 6	Sky	128	255	255
% 7	Shop	0	255	0
% 8	Shutter	128	128	0
% 9	Road	128	128	128

labelmap = [
0	0	0	;
255	0	0	;
255	255	0	;
128	0	255	;
255	128	0	;
0	0	255	;
128	255	255	;
0	255	0	;
128	128	0	;
128	128	128	;
]/255;
